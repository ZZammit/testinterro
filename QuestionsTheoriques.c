#include <stdio.h>
#include <stdlib.h>


int main()
{
    //Question 1:


    //La boucle while qui compte de 1 � 10
    int compteur = 1;

    while (compteur <10+1)
    {
        printf("Le compteur while : %d\n", compteur++);
    }

    //La boucle for qui compte de 1 � 10
    int compt;

    for (compt = 1 ; compt < 10+1 ; compt ++)
    {
        printf("    Le compteur for: %d\n", compt);
    }




    //Question 2:


    // Une constante prend une valeur qui ne change pas, cette valeur est stock�e dans la m�moire.
    //#define prend la valeur de la variable qui peut changer da valeur dans la programme




    //Question 3:

    //Tarif des inscriptions � des courses � pied

    //D�claration de la variable
    int distance;

    //Un menu pour le choix
    printf("\n\n\n");
    printf("Choix de la distance  : \n");
    printf("1. Les foulee de la jenesse (distance libre)  \n");
    printf("2. Les 5 km  \n");
    printf("3. Les 10 km  \n");
    printf("4. Le semi-marathon - 21 km  \n");
    printf("5. Le marathon - 42 km  \n\n");
    printf("Votre choix : ");

    scanf("%d", &distance);



    // Exemple avec if ... else if ... else

    if (distance == 1)
    {
        printf("Le tarif de l'inscription a la distance libre est de 15 euros ");
    }

    else if (distance == 2)
    {
        printf("Le tarif de l'inscription a la course de 5 km est de 15 euros ");
    }

    else if (distance == 3)
    {
        printf("Le tarif de l'inscription a la course de 5 km est de 25 euros ");
    }

    else if (distance == 4)
    {
        printf("Le tarif de l'inscription a la course de 5 km est de 35 euros ");
    }

    else if (distance == 5)
    {
        printf("Le tarif de l'inscription a la course de 5 km est de 50 euros ");
    }

    else
    {
        printf("Preparez vous pour mieux l'annee prochaine !");
    }


//Exemple avec avec switch

    //Un menu pour le choix
    printf("\n\n\n");
    printf("Choix de la distance  : \n");
    printf("1. Les foulee de la jenesse (distance libre)  \n");
    printf("2. Les 5 km  \n");
    printf("3. Les 10 km  \n");
    printf("4. Le semi-marathon - 21 km  \n");
    printf("5. Le marathon - 42 km  \n\n");
    printf("Votre choix : ");

    scanf("%d", &distance);

    switch (distance)
    {
    case 1:
    case 2:
        printf("Le tarif de l'inscription est de 15 euros\n\n");
        break;

    case 3:
        printf("Le tarif de l'inscription est de 25 euros\n\n");
        break;

    case 4:
        printf("Le tarif de l'inscription est de 35 euros\n\n");
        break;

    case 5:
        printf("Le tarif de l'inscription est de 50 euros\n\n");
        break;

    default:
        printf("Preparez vous mieux pour l'annee prochaine !\n\n");

    }
    return 0;
}
